'use strict';
var LIVERELOAD_PORT = 35729;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

module.exports = function(grunt) {
  // Load all Grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
    sass: {
      dist: {
        files: {
          "app/styles/main.css": "app/styles/scss/main.scss"
        }
      }
    },
    cssmin: {
      options: {
        keepBreaks: false,
        keepSpecialComments: 0
      },
      minify: {
        files: [{
          expand: true,
          cwd: 'app/styles/',
          src: ['*.css'],
          dest: 'app/styles/',
          ext: '.min.css'
        }]
      }
    },
    uglify: {
      options: {
        mangle: true,
        sourceMap: true
      },
      build: {
        files: [{
          expand: true,
          src: 'app/scripts/src/{,*/}*.js',
          dest: 'app/scripts/dist',
          flatten: true
        }]
      }
    },
    clean: {
      css: ["app/styles/*.css", "app/styles/*.map"],
      js: ["app/scripts/dist/*.*"]
    },
    copy: {
      lib: {
        expand: true,
        flatten: true,
        files: {
          // JavaScript
          'app/scripts/dist/require.js':      ['app/bower_components/requirejs/require.js']
        }
      },
    },
    watch: {
      css: {
        files: "**/*.scss",
        tasks: [ "build" ],
        options: {
          livereload: true
        }
      },
      js: {
        files: [ "app/scripts/src/**/*.js", "app/config/*.js" ],
        tasks: [ "build" ],
        options: {
          livereload: true
        }
      },
      templates: {
        files: [ "app/templates/*.html" ],
        tasks: [ "build" ],
        options: {
          livereload: true
        }
      },
      images: {
        files: [ "app/images/**/*.*" ],
        tasks: [ "build" ],
        options: {
          livereload: true
        }
      }
    },
    open: {
      all: {
        path: 'http://localhost:9000'
      }
    },
    connect: {
      options: {
        port: 9000,
        // change this to '0.0.0.0' to access the server from outside
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              mountFolder(connect, 'app'),
              lrSnippet
            ];
          }
        }
      }
    },
  })

  grunt.registerTask('build', function (target) {
    grunt.task.run([
      'clean:css',
      'sass',
      'cssmin',
      'uglify',
      'copy:lib'
    ]);
  });

  grunt.registerTask('server', function (target) {
    grunt.task.run([
      'build',
      'connect:livereload',
      'open',
      'watch'
    ]);
  });

}
