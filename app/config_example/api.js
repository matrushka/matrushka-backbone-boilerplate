define(function() {
  var conf = {
    api: {
      host: "HOST",
      port: 80,
      prefix: '/api/v2',
      environment: 'test',
      requireToken: false
    }
  }
  var a = conf.api
  a.baseURL = 'http://' + a.host + ':' + a.port + a.prefix;

  return conf
})
