define([
  'jquery',
  'config/api',
  'config/paths'
],
function($, api, paths) {
  var config = {
    lang: {
      default: 'en'
    },
    debug: true
  }

  config = $.extend(config, api, paths);

  // Register globally
  config.register = function(globalName) {
    if(typeof(globalName) == 'undefined') {
      globalName = 'c'
    }

    window[globalName] = this
  }

  return config
})
