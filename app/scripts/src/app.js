define(['router', 'mainview'], function(Router, MainView) {

  var App = {
    initialize: function() {
      Router.initialize()
      
      App.mainView   = new MainView;
      App.router     = Router;
    },
    register: function(globalName) {
      if(typeof(globalName) == 'undefined') {
        globalName = 'App'
      }

      window[globalName] = this
    }
  }

  return App;
})
