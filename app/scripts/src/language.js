define([
  'jquery',
  'jquery.cookie',
  'underi18n',
  'languages/en',
  'languages/es',
  'languages/fr'  
],
function($, cookie, underi18n, en, es, fr) {
  $.removeCookie('lang')
  var t_ = {
    en: underi18n.MessageFactory(en),
    es: underi18n.MessageFactory(es),
    fr: underi18n.MessageFactory(fr),
  }

  var t = function(code, data) {
    return t_[t.getLanguage()](code, data)
  }

  t.switchLanguage = function(languageCode) {
    $.cookie('lang', languageCode)
  }

  t.getLanguage = function() {
    var lang = $.cookie('lang')
    
    if(typeof(lang) == 'undefined') {
      t.switchLanguage(C.lang.default)
      return C.lang.default
    }
    
    return lang
  }

  // Register globally
  t.register = function(globalName) {
    if(typeof(globalName) == 'undefined') {
      globalName = 't'
    }

    window[globalName] = this
  }

  return t
})
