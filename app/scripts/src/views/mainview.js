define([
  'backbone',

  'text!templates/scaffold.html',
], 

function(Backbone, scaffold_t) {
  var MainView = Backbone.View.extend({
    el: $('#app_container'),
    tagName: 'div',
    className: 'main-view',

    events: {

    },

    initialize: function() {
      this.render()
    },

    render: function() {
      var data = {name: 'Some Data'}
      
      this.$el.empty().append($$(
      {
        name: 'scaffold',
        content: scaffold_t,
      }, data))
    }
  })

  return MainView
})
