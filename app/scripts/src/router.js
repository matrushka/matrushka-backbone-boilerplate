define([
  'backbone',
  'scaffold'
], 

function(Backbone, Scaffold) {
  var AppRouter = Backbone.Router.extend({
    routes: {
      'test': 'testRouter',
    },

    testRouter: function() {
      Scaffold.test()
    },
  })
  
  var initialize = function() {
    var app_router = new AppRouter

    // app_router.on('route:testRouter', function() {
    //   console.log('Testing router from initialize binding')
    // })

    Backbone.history.start()
  }

  return {initialize: initialize}
})
