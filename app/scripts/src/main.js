requirejs.config({
  paths: {
    templates:  '../../templates',
    languages:  '../../languages',
    config:     '../../config',

    jquery:     '../../bower_components/jquery/dist/jquery.min',
    backbone:   '../../bower_components/backbone/backbone',
    underscore: '../../bower_components/underscore/underscore-min',
    'jquery.cookie': '../../bower_components/jquery-cookie/jquery.cookie',
    underi18n:  '../../bower_components/underi18n/underi18n',
    text:       '../../bower_components/requirejs-text/text'
  }
})

require([
  'config/config',
  'app', 
  'templated',
  'language',
  'scaffold'
], 

function(Config, App, Templated, Language, Scaffold) {
  Templated.init('$$')    // Register the templating function globally
  Config.register('C')    // Register the configuration object globally
  Language.register('t')  // Register the translation function globally
  
  App.register()
  App.initialize();
})
