# Matrushka Boilerplate

## 1. Install Dependencies

 - [Bower](http://bower.io/#install-bower)
 - [Grunt CLI](http://gruntjs.com/getting-started)
 - [Sass](http://sass-lang.com/install)

## Install Bower components and Node modules
```bash
bower install
npm install
```
## Create configuration files
You can find examples of configuration files in `app/config_example`
 - `app/config/config.js // Main configuration file, loads other configuration files`
 - `app/config/paths.js`
 - `app/config/api.js`
